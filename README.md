# ansible-jumpserver

#### 介绍
使用 [Ansible](https://www.ansible.com) 自动化部署软堡垒机 [jumpserver](https://github.com/jumpserver/jumpserver)

#### 软件架构
目前只支持单机部署。Redis 和数据库可以使用外部连接。

测试通过

| 系统/软件 | 版本   |
| --------- | ------ |
| Ubuntu    | xenial |
| CentOS    | 7      |
| Ansible   | 2.7.5  |
| python    | 3.6    |

#### 安装教程
##### 准备前

1. 更新你的目标机系统。
1. 执行 `sh bootstrap-ansible.sh`，安装 ansible。

##### 修改 hosts 和 group_vars

编辑 `hosts.ini`，就修改 jms 的连接信息就好了。

```
$ cp hosts_example.ini hosts.ini
$ vi hosts.ini
```

编辑 `group_vars/all.yml`，主要是 jms_zone 的名称，用于识别目标区(例如不同的机房或网域)。

##### 初次部署

可以直接执行 `ansible-playbook play-all.yml` 完成。

##### 分步部署

1. 在 vars 目录下找到同名变量文件，例如 00-init.yml 对应的变量配置在 vars/init.yml 下，按实际需求修改。
1. 在 templates 目录下找到同名子目录，编辑对应的模板文件。例如 00-init.yml 对应的模板文件存放在 templates/init 目录下。
1. 用 ansible-playbook 命令执行对应的 playbook 即可。例如 `ansible-playbook 00-init.yml`
1. 对变量和模板的内容不理解的，请参考 [官方文档](http://docs.jumpserver.org/zh/docs/index.html) 的说明。

#### 使用说明

1. 部署过程中生成的密码都存放在 credentials 目录下。
1. [Jumpserver 管理文档](http://docs.jumpserver.org/zh/docs/admin_guide.html)
1. [Jumpserver 用户使用文档](http://docs.jumpserver.org/zh/docs/user_guide.html)

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
